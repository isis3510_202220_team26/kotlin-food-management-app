package com.foodia.recipeapp

import android.content.Intent
import android.os.Bundle
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.isVisible
import androidx.lifecycle.lifecycleScope
import androidx.paging.LoadState
import com.foodia.recipeapp.adapter.ShoppingListAdapter
import com.foodia.recipeapp.databinding.ActivityShoppingListBinding
import com.foodia.recipeapp.vm.ShoppingListViewModel
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch

class MainActivity : AppCompatActivity() {

    private lateinit var dataBinding: ActivityShoppingListBinding
    private val adapter = ShoppingListAdapter()
    private val viewModel by viewModels<ShoppingListViewModel>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        dataBinding = ActivityShoppingListBinding.inflate(layoutInflater)
        setContentView(dataBinding.root)
        setProductsAdapter()
        getProducts()
        setProgressBarAccordingToLoadState()
    }

    private fun setProductsAdapter() {
        dataBinding.pantryRecyclerView.adapter = adapter
    }

    private fun getProducts() {
        lifecycleScope.launch {
            viewModel.flow.collectLatest{
                val t = it
                adapter.submitData(t)
            }
        }
    }

    private fun setProgressBarAccordingToLoadState() {
        lifecycleScope.launch {
            adapter.loadStateFlow.collectLatest{
                dataBinding.progressBar.isVisible = it.append is LoadState.Loading
            }
        }
    }
}