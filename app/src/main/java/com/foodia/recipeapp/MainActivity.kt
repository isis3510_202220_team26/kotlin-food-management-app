package com.foodia.recipeapp

//import com.example.firebaseloginkotlin.databinding.ActivityMainBinding
import android.content.Context
import android.content.Intent
import android.net.ConnectivityManager
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.util.Log.d
import android.view.ViewGroup
import android.widget.Button
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.room.Room
import com.bumptech.glide.Glide
import com.foodia.recipeapp.database.RecipeDatabase
import com.foodia.recipeapp.database.RecipeFromDatabase
import com.foodia.recipeapp.databinding.ActivityMainBinding
import com.foodia.recipeapp.entities.UserInfo
import com.foodia.recipeapp.firebase.UsersCollection
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.ktx.auth
import com.google.firebase.auth.ktx.userProfileChangeRequest
import com.google.firebase.firestore.ktx.toObject
import com.google.firebase.ktx.Firebase
import com.google.firebase.storage.FirebaseStorage
import com.google.firebase.storage.StorageReference
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread

@Suppress("DEPRECATION")
class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding
    private lateinit var auth: FirebaseAuth
    private val fileResult = 1
    private val userCollection = UsersCollection()
    private val TAG = "MainActivity"



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        /*val crashButton = Button(this)
        crashButton.text = "Test Crash"
        crashButton.setOnClickListener {
            throw RuntimeException("Test Crash") // Force a crash
        }

        addContentView(crashButton, ViewGroup.LayoutParams(
            ViewGroup.LayoutParams.MATCH_PARENT,
            ViewGroup.LayoutParams.WRAP_CONTENT))*/

        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        auth = Firebase.auth

        updateUI()

        binding.updateProfileAppCompatButton.setOnClickListener {
            updateProfile()
        }

        binding.profileImageView.setOnClickListener {
            fileManager()
        }


        binding.updatePasswordTextView.setOnClickListener {
            val intent = Intent(this, UpdatePasswordActivity::class.java)
            this.startActivity(intent)
        }
        binding.recipesButton.setOnClickListener {
            if (isOnline()) {
                val intent = Intent( this, RecipesActivity::class.java)
                this.startActivity(intent)
            }
            else
            {
                val intent = Intent(this, LocalRecipesActivity::class.java)
                this.startActivity(intent)
                val toast = Toast.makeText(this, "No connection, showing locally saved recipes", Toast.LENGTH_LONG)
                toast.show()
            }


        }

        binding.signOutImageView.setOnClickListener {
            signOut()
        }

        binding.signOutImageView.setOnClickListener {
            signOut()
        }

        binding.openPantryView.setOnClickListener {
            this.startActivity(Intent(this, PantryActivity::class.java))
        }

    }

    private  fun updateProfile () {

        val user = auth.currentUser
        val firstName = binding.firstNameEditText.text.toString()
        val lastName = binding.lastNameEditText.text.toString()
        val email = binding.emailEditText.text.toString()
        val dailyCalories = binding.dailyCaloriesEditText.text.toString().toInt()
        val updatedUserInfo = UserInfo(firstName, lastName, email, dailyCalories, 0, "" )

        val profileUpdates = userProfileChangeRequest {
            displayName = "$firstName $lastName"
        }


        if (user != null) {
            user.email?.let { userCollection.updateUserByEmail(it, updatedUserInfo) }
        }

        user!!.updateProfile(profileUpdates)
            .addOnCompleteListener { task ->
                if (task.isSuccessful) {
                    Toast.makeText(this, "Se realizaron los cambios correctamente.",
                        Toast.LENGTH_SHORT).show()
                    updateUI()
                }
            }
    }

    private fun fileManager() {
        val intent = Intent(Intent.ACTION_GET_CONTENT)
        intent.type = "image/*"
        startActivityForResult(intent, fileResult)
    }

    @Deprecated("Deprecated in Java")
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == fileResult) {
            if (resultCode == RESULT_OK && data != null) {
                val uri = data.data

                uri?.let { imageUpload(it) }

            }
        }
    }

    private fun imageUpload(mUri: Uri) {

        val user = auth.currentUser
        val folder: StorageReference = FirebaseStorage.getInstance().reference.child("Users")
        val fileName: StorageReference = folder.child("img"+user!!.uid)

        fileName.putFile(mUri).addOnSuccessListener {
            fileName.downloadUrl.addOnSuccessListener { uri ->

                val profileUpdates = userProfileChangeRequest {
                    photoUri = Uri.parse(uri.toString())
                }

                user.updateProfile(profileUpdates)
                    .addOnCompleteListener { task ->
                        if (task.isSuccessful) {
                            Toast.makeText(this, "Se realizaron los cambios correctamente.",
                                Toast.LENGTH_SHORT).show()
                            updateUI()
                        }
                    }
            }
        }.addOnFailureListener {
            Log.i("TAG", "file upload error")
        }
    }



    private  fun updateUI () {
        val user = auth.currentUser
        val docRef = user?.email?.let { userCollection.getDb().collection("Users").document(user.email!!) }
        docRef?.get()?.addOnSuccessListener{ document ->
            if (document != null) {
                val userInfo = document.toObject<UserInfo>()
                Log.d(TAG, "DocumentSnapshot data: $userInfo")
                binding.emailTextView.text = user.email
                binding.nameTextView.text = user.displayName
                if (userInfo != null) {
                    binding.firstNameTextView.text = userInfo.firstName
                    binding.lastNameTextView.text = userInfo.lastName
                    binding.dailyCaloriesTextView.text = userInfo.calories.toString()

                    binding.emailEditText.setText(user.email)
                    binding.firstNameEditText.setText(userInfo.firstName)
                    binding.lastNameEditText.setText(userInfo.lastName)
                    binding.dailyCaloriesEditText.setText(userInfo.calories.toString())
                }


                Glide
                    .with(this)
                    .load(user.photoUrl)
                    .centerCrop()
                    .placeholder(R.drawable.ic_profile)
                    .into(binding.profileImageView)
                Glide
                    .with(this)
                    .load(user.photoUrl)
                    .centerCrop()
                    .placeholder(R.drawable.ic_profile)
                    .into(binding.bgProfileImageView)
            } else {
                Log.d(TAG, "No such document")
            }
        }?.addOnFailureListener { e ->
            Log.w(TAG, "Error getting document", e)
        }

    }

    private  fun signOut(){
        auth.signOut()
        val intent = Intent(this, SignInActivity::class.java)
        this.startActivity(intent)
    }

    private fun isOnline():Boolean{
        val conManager = getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val internetInfo =conManager.activeNetworkInfo
        return internetInfo!=null && internetInfo.isConnected
    }
}