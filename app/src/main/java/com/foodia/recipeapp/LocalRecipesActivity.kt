package com.foodia.recipeapp


import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.graphics.Typeface
import android.net.ConnectivityManager
import android.os.Bundle
import android.util.Log
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.marginLeft
import androidx.lifecycle.lifecycleScope
import com.foodia.recipeapp.databinding.ActivityLocalRecipesBinding
import kotlinx.android.synthetic.main.activity_local_recipes.*
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.coroutines.*

class LocalRecipesActivity : AppCompatActivity() {

    private lateinit var binding: ActivityLocalRecipesBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityLocalRecipesBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.imageViewUserBtn.setOnClickListener {
            val intent = Intent(this, MainActivity::class.java)
            this.startActivity(intent)
        }
        binding.onlinemessage.setOnClickListener {
            val intent = Intent(this, RecipesActivity::class.java)
            this.startActivity(intent)
        }
        binding.CreateRecipeButton.setOnClickListener {
            val intent = Intent(this, CreateRecipeActivity::class.java)
            this.startActivity(intent)
        }

        val sharedPrefFile = "kotlinsharedpreference"

        val sharedPreferences: SharedPreferences =
            this.getSharedPreferences(sharedPrefFile, Context.MODE_PRIVATE)

        val allEntries: Map<String, *> = sharedPreferences.all
        allEntries.forEach { entry ->
            Log.d("entries", "${entry.key} : ${entry.value}")
            var textview = TextView(this)
            textview.textSize = 15F
            textview.setTypeface(Typeface.DEFAULT_BOLD)
            textview.text = "${entry.key}"
//            textview.visibility =  android.view.View.VISIBLE
            localRecipesLayout.addView(textview)

            var textview2 = TextView(this)
            textview2.textSize = 15F
            textview2.setTypeface(Typeface.DEFAULT)
            textview2.text = "${entry.value}"
//            textview.visibility =  android.view.View.VISIBLE
            localRecipesLayout.addView(textview2)
        }

       checkIfOnline()



    }

    private fun checkIfOnline()  {
        lifecycleScope.launch{
           while (!isOnline()) {
                delay(5000)
            }
            onlinemessage.visibility = android.view.View.VISIBLE
        }


    }

    private fun isOnline(): Boolean {
        val conManager = getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val internetInfo = conManager.activeNetworkInfo
        return internetInfo != null && internetInfo.isConnected
    }
}