package com.foodia.recipeapp

import android.content.Intent
import android.os.Bundle
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.isVisible
import androidx.lifecycle.lifecycleScope
import androidx.paging.LoadState
import com.foodia.recipeapp.adapter.PantryAdapter
import com.foodia.recipeapp.adapter.ShoppingListAdapter
import com.foodia.recipeapp.databinding.ActivityPantryBinding
import com.foodia.recipeapp.databinding.ActivityShoppingListBinding
import com.foodia.recipeapp.vm.PantryViewModel
import com.foodia.recipeapp.vm.ShoppingListViewModel
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.SupervisorJob
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch

class ShoppingListActivity : AppCompatActivity() {

    private lateinit var dataBinding: ActivityShoppingListBinding
    private val adapter = ShoppingListAdapter()
    private val viewModel by viewModels<ShoppingListViewModel>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        dataBinding = ActivityShoppingListBinding.inflate(layoutInflater)
        setContentView(dataBinding.root)
        setProductsAdapter()
        getProducts()
        setProgressBarAccordingToLoadState()

        dataBinding.backToProfile.setOnClickListener {
            this.onBackPressed()
        }

        dataBinding.backToProfile3.setOnClickListener{
            this.startActivity(Intent(this, AddItemShoppingActivity::class.java))
        }
    }

    private fun setProductsAdapter() {
        dataBinding.pantryRecyclerView.adapter = adapter
    }

    private fun getProducts() {
        CoroutineScope(Dispatchers.IO + SupervisorJob()).launch {
            viewModel.flow.collectLatest{
                val t = it
                adapter.submitData(t)
            }
        }
    }

    private fun setProgressBarAccordingToLoadState() {


        CoroutineScope(Dispatchers.IO + SupervisorJob()).launch {
            adapter.loadStateFlow.collectLatest{
                dataBinding.progressBar.isVisible = it.append is LoadState.Loading
            }
        }
    }

}