package com.foodia.recipeapp

import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.isVisible
import com.foodia.recipeapp.shared.isNetworkConnected
import androidx.lifecycle.lifecycleScope
import androidx.paging.LoadState
import com.foodia.recipeapp.adapter.PantryAdapter
import com.foodia.recipeapp.databinding.ActivityPantryBinding
import com.foodia.recipeapp.vm.PantryViewModel
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.SupervisorJob
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch

class PantryActivity : AppCompatActivity() {

    private lateinit var dataBinding: ActivityPantryBinding
    private val adapter = PantryAdapter()
    private val viewModel by viewModels<PantryViewModel>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        dataBinding = ActivityPantryBinding.inflate(layoutInflater)
        setContentView(dataBinding.root)
        setProductsAdapter()
        getProducts()
        setProgressBarAccordingToLoadState()

        dataBinding.backToProfile.setOnClickListener {
            this.startActivity(Intent(this, AddItemActivity::class.java))
        }

        dataBinding.backToProfile2.setOnClickListener {
            this.startActivity(Intent(this, ShoppingListActivity::class.java))
        }
    }

    private fun setProductsAdapter() {
        dataBinding.pantryRecyclerView.adapter = adapter
    }

    private fun getProducts() {
        val c = this;
        CoroutineScope(Dispatchers.IO + SupervisorJob()).launch {
            viewModel.flow.collectLatest{
                val t = it
                if(isNetworkConnected){
                    adapter.submitData(t)
                }else{
                    Toast.makeText(c, "No hay conexíon a internet.",
                        Toast.LENGTH_SHORT).show()
                }

            }
        }
    }

    private fun setProgressBarAccordingToLoadState() {


        CoroutineScope(Dispatchers.IO + SupervisorJob()).launch {
            adapter.loadStateFlow.collectLatest{
                dataBinding.progressBar.isVisible = it.append is LoadState.Loading
            }
        }
    }

}