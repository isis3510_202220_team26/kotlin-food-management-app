package com.foodia.recipeapp

// Firebase Firestore
import android.content.Intent
import android.os.Bundle
import android.text.TextUtils
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.FragmentActivity
import com.foodia.recipeapp.databinding.ActivityAddItemBinding
import com.foodia.recipeapp.databinding.ActivityAddItemShoppingBinding
import com.foodia.recipeapp.entities.PantryItem
import com.foodia.recipeapp.firebase.PantryCollection
import com.google.firebase.auth.ktx.auth
import com.google.firebase.firestore.SetOptions
import com.google.firebase.ktx.Firebase
import java.text.SimpleDateFormat
import java.util.UUID

//import com.google.firebase.database.DatabaseReference
//import com.google.firebase.database.FirebaseDatabase

class AddItemShoppingActivity : AppCompatActivity() {

    private lateinit var binding: ActivityAddItemShoppingBinding

    private val pantryCollection = PantryCollection()


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityAddItemShoppingBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.additembuttontodatabase.setOnClickListener(View.OnClickListener { addItem() })
    }

    // addding item to databse
    private fun addItem() {

        //data
        val nameValue =  binding.edititemname.text.toString()
        val measurementValue = binding.editcategory.text.toString()
        val quantityValue =  binding.editprice.text.toString()
        val expireDateValue = "31/12/2024"
        val itemBarcodeValue = UUID.randomUUID().toString()

        if (!TextUtils.isEmpty(nameValue) && !TextUtils.isEmpty(measurementValue) && !TextUtils.isEmpty(expireDateValue)&& !TextUtils.isEmpty(quantityValue)
        ) {
            val user: String = Firebase.auth.currentUser?.email?: "no user"
            val item = PantryItem(
                expireDateValue,
                measurementValue,
                nameValue,
                quantityValue.toLong(),
                user,
                false,
                itemBarcodeValue
            )
            pantryCollection.getDb().collection(pantryCollection.getCollectionName())
                .document(itemBarcodeValue)
                .set(item, SetOptions.merge())

            binding.edititemname.text.clear()
            binding.editcategory.text.clear()
            binding.editprice.text.clear()


            Toast.makeText(this, "$nameValue Added", Toast.LENGTH_SHORT).show()
        } else {
            Toast.makeText(this, "Please Fill all the fields", Toast.LENGTH_SHORT)
                .show()
        }
    }



}