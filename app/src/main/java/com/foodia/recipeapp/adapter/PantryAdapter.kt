package com.foodia.recipeapp.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.paging.PagingDataAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.foodia.recipeapp.R
import com.foodia.recipeapp.databinding.PantryItemBinding
import com.foodia.recipeapp.entities.PantryItem
import kotlinx.android.synthetic.main.pantry_item.view.*

class PantryAdapter: PagingDataAdapter<PantryItem, PantryAdapter.PantryViewHolder>(Companion) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PantryViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val dataBinding = PantryItemBinding.inflate(layoutInflater, parent, false)
        return PantryViewHolder(dataBinding)
    }

    override fun onBindViewHolder(holder: PantryViewHolder, position: Int) {
        val pantry = getItem(position)?:return
        holder.bindPantry(pantry)
    }


    companion object: DiffUtil.ItemCallback<PantryItem>() {
        override fun areItemsTheSame(oldItem: PantryItem, newItem: PantryItem): Boolean {
            return oldItem.name == newItem.name
        }

        override fun areContentsTheSame(oldItem: PantryItem, newItem: PantryItem): Boolean {
            return oldItem == newItem
        }
    }
    inner class PantryViewHolder(
        private val dataBinding: PantryItemBinding
    ): RecyclerView.ViewHolder(dataBinding.root){
        fun bindPantry(pantry: PantryItem) {
            dataBinding.pantryItemExpiryDate.text = pantry.expireDate
            dataBinding.pantryItemMeasurement.text = pantry.measurement
            dataBinding.pantryItemName.text = pantry.name
            dataBinding.pantryItemQuantity.text = pantry.quantity.toString()
        }
    }

}

