package com.foodia.recipeapp.firebase

import android.util.Log
import com.foodia.recipeapp.entities.UserInfo
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase

class UsersCollection {

    private var db = Firebase.firestore
    private val TAG = "UsersCollection"
    private val collectionName = "Users"

    fun getDb(): FirebaseFirestore {
        return db
    }

    fun updateUserByEmail(email: String, newInfo: UserInfo) {
        val docRef = db.collection(collectionName).document(email)
        docRef
            .update(
                "firstName", newInfo.firstName,
                "lastName", newInfo.lastName,
                "email", newInfo.email,
                "calories", newInfo.calories,
            )
            .addOnSuccessListener { Log.d(TAG, "DocumentSnapshot successfully updated!") }
            .addOnFailureListener { e ->
                Log.w(TAG, "Error updating document", e)
            }
    }

    fun createUser(email: String) {
        db.collection(collectionName).document(email).set(UsersCollection())
    }
}