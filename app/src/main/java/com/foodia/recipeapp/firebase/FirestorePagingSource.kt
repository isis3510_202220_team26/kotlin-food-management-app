package com.foodia.recipeapp.firebase

import androidx.paging.PagingSource
import androidx.paging.PagingState
import com.foodia.recipeapp.entities.PantryItem
import com.google.firebase.firestore.Query
import com.google.firebase.firestore.QuerySnapshot
import kotlinx.coroutines.tasks.await

class FirestorePagingSource (
    private val queryPantryItems: Query
) : PagingSource<QuerySnapshot, PantryItem>() {
    override fun getRefreshKey(state: PagingState<QuerySnapshot, PantryItem>): QuerySnapshot? {
        return null
    }

    override suspend fun load(params: LoadParams<QuerySnapshot>): LoadResult<QuerySnapshot, PantryItem> {
        return try {
            val currentPage = params.key ?: queryPantryItems.get().await()
            val lastVisiblePantryItem = currentPage.documents[currentPage.size() - 1]
            val nextPage = queryPantryItems.startAfter(lastVisiblePantryItem).get().await()
            LoadResult.Page(
                data = currentPage.toObjects(PantryItem::class.java),
                prevKey = null,
                nextKey = nextPage
            )
        } catch (e: Exception) {
            LoadResult.Error(e)
        }
    }
}