package com.foodia.recipeapp.firebase

import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase

class PantryCollection {
    private var db = Firebase.firestore
    private val collectionName = "pantry"

    fun getDb(): FirebaseFirestore {
        return db
    }

    fun getCollectionName(): String {
        return collectionName
    }

    fun updatePantryItem(
        id:String,
        name: String,
        quantity: Long,
        measurement: String,
        expireDate: String,
        b: Boolean
    ) {
        val pantryItem = hashMapOf(
            "name" to name,
            "quantity" to quantity,
            "measurement" to measurement,
            "expireDate" to expireDate,
            "purchased" to b
        )

        db.collection(collectionName).document(id).set(pantryItem)
    }
}