package com.foodia.recipeapp.vm

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.paging.Pager
import androidx.paging.PagingConfig
import androidx.paging.cachedIn
import com.foodia.recipeapp.firebase.FirestorePagingSource
import com.google.firebase.auth.ktx.auth
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.Query
import com.google.firebase.ktx.Firebase
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class PantryViewModel @Inject constructor(
) : ViewModel() {
    val flow = Pager(
        PagingConfig(
            pageSize = 20
        )
    ) {
        val email = Firebase.auth.currentUser?.email
        println("email: $email")
        val queryProductsByName = FirebaseFirestore.getInstance()
            .collection("pantry")
            .whereEqualTo("user", email)
            .whereEqualTo("purchased", true)
            .orderBy("expireDate", Query.Direction.ASCENDING)
            .limit("20".toLong())
        FirestorePagingSource(queryProductsByName)
    }.flow.cachedIn(viewModelScope)
}