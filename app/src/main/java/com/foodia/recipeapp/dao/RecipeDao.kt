package com.foodia.recipeapp.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import com.foodia.recipeapp.database.RecipeFromDatabase

@Dao
interface RecipeDao {

    @Query("SELECT * FROM RecipeFromDatabase")
    fun getAll(): List<RecipeFromDatabase>

    @Insert
    fun insert(vararg recipes: RecipeFromDatabase)

}