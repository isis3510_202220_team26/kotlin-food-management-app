package com.foodia.recipeapp


import android.content.ClipDescription
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.net.ConnectivityManager
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.lifecycleScope
import androidx.room.Room
import com.bumptech.glide.Glide
import com.foodia.recipeapp.database.RecipeDatabase
import com.foodia.recipeapp.database.RecipeFromDatabase
import com.foodia.recipeapp.databinding.ActivityCreateRecipeBinding
import com.foodia.recipeapp.databinding.ActivityRecipesBinding
import com.google.firebase.Timestamp
import com.google.firebase.firestore.DocumentReference
import com.google.firebase.firestore.Query
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase
import kotlinx.android.synthetic.main.activity_create_recipe.*
import kotlinx.android.synthetic.main.activity_local_recipes.*
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread
import java.util.*
import kotlin.collections.ArrayList


class CreateRecipeActivity : AppCompatActivity() {

    private lateinit var binding: ActivityCreateRecipeBinding


    val creAct = this
    val db = Firebase.firestore
    var recetasOrdenLikes = arrayOf<Map<String, Any>>()
    var recetasOrdenNombre = arrayOf<Map<String, Any>>()
    val toBeUploadedFile = "recipestobeuploaded"
    var mostUsedIngredient: String = ""

// ...


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val sharedPrefFile = "kotlinsharedpreference"

        val sharedPreferences: SharedPreferences =
            this.getSharedPreferences(sharedPrefFile, Context.MODE_PRIVATE)

        val toBeUploadedPreferences: SharedPreferences =
            this.getSharedPreferences(toBeUploadedFile, Context.MODE_PRIVATE)

        binding = ActivityCreateRecipeBinding.inflate(layoutInflater)
        setContentView(binding.root)

        val list: MutableList<String> = ArrayList()
        list.add("Chicken")
        list.add("Beef")
        list.add("Fish")
        list.add("pork")
        list.add("Pasta")
        list.add("Other")

        val adapter =
            ArrayAdapter<String>(this, R.layout.support_simple_spinner_dropdown_item, list)

        mainIngredientSpinner.adapter = adapter

//        mainIngredientSpinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener{
//            override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
//                val item = list[p2]
//                Toast.makeText(this@CreateRecipeActivity, "$item", Toast.LENGTH_LONG).show()
//            }
//
//            override fun onNothingSelected(p0: AdapterView<*>?) {}
//
//        }

        var valMax = 0
        var valActual = 0
        db.collection("main_ingredient").get().addOnSuccessListener { result ->
            for (document in result) {
                valActual = document.data["amount"].toString().toInt()
                if (valMax < valActual)
                {
                    mostUsedIngredient = document.id
                    valMax = valActual
                }
            }
            binding.mostUsedIngredientText.text = "Select the main ingredient, the most common is "+mostUsedIngredient

        }
        binding.createButton.setOnClickListener {

            var title = binding.titleEditText.text.toString()
            var description = binding.descriptionEditText.text.toString()
            var mainIngredient = binding.mainIngredientSpinner.selectedItem.toString()
            if (isOnline()) {
                val intent = Intent(this, RecipesActivity::class.java)
                uploadRecipe(title, description, mainIngredient)

                val toast = Toast.makeText(
                    this,
                    "Recipe created and posted, you can see it in all recipes section",
                    Toast.LENGTH_LONG
                )
                toast.show()
            } else {
                val editor: SharedPreferences.Editor = sharedPreferences.edit()
                editor.putString(title, description)
                editor.apply()
                editor.commit()
                val editorToBeUploaded: SharedPreferences.Editor = toBeUploadedPreferences.edit()
                editorToBeUploaded.putString(title, description)
                editorToBeUploaded.apply()
                editorToBeUploaded.commit()
                val intent = Intent(this, LocalRecipesActivity::class.java)
                this.startActivity(intent)
                val toast = Toast.makeText(
                    this,
                    "No connection, saving locally and waiting for connection",
                    Toast.LENGTH_LONG
                )
                toast.show()
                checkIfOnline(mainIngredient)
            }

        }


    }

    private fun isOnline(): Boolean {
        val conManager = getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val internetInfo = conManager.activeNetworkInfo
        return internetInfo != null && internetInfo.isConnected
    }

    private fun checkIfOnline(mainIngredient: String) {
        val toBeUploadedPreferences: SharedPreferences =
            this.getSharedPreferences(toBeUploadedFile, Context.MODE_PRIVATE)


        lifecycleScope.launch {
            while (!isOnline()) {
                delay(5000)
            }
            val allEntries: Map<String, *> = toBeUploadedPreferences.all
            val editor: SharedPreferences.Editor = toBeUploadedPreferences.edit()
            var title: String = ""
            for (allEntry in allEntries) {

                title = allEntry.key
                uploadRecipe(allEntry.key, allEntry.value.toString(), mainIngredient)
                editor.remove(allEntry.key)
                editor.apply()
                editor.commit()
            }
            val toast = Toast.makeText(
                creAct,
                "$title has been uploaded, look for it!",
                Toast.LENGTH_LONG
            )
            toast.show()

        }


    }

    fun limitDropdownHeight(spinner: Spinner) {
        val popup = Spinner::class.java.getDeclaredField("mPopup")
        popup.isAccessible = true
        val popupWindow = popup.get(spinner) as ListPopupWindow
        popupWindow.height = (100 * resources.displayMetrics.density).toInt()
    }

    private fun uploadRecipe(title: String, description: String, mainIngredient: String) {
        val intent = Intent(this, RecipesActivity::class.java)
        doAsync {

            val dataRecipe = hashMapOf(
                "dateCreated" to Timestamp(Date()),
                "description" to description,
                "imageURL" to "https://i.pinimg.com/736x/b8/72/26/b87226fa0ee9e34158ddf083d5ce74a1.jpg",
                "numberOfLikes" to 0,
                "title" to title
            )
            db.collection("recipes").document(title).set(dataRecipe)
            val docRef = db.collection("main_ingredient").document(mainIngredient)
            docRef.get()
                .addOnSuccessListener { document ->
                    if (document.data != null) {
                        Log.d("ipeem", "DocumentSnapshot data: ${document.data}")

                        var valor: Int = document.data!!["amount"].toString().toInt()
                        val dataSet = hashMapOf(
                            "amount" to valor + 1
                        )
                        db.collection("main_ingredient").document(mainIngredient).set(dataSet)
                    } else {
                        val number = hashMapOf(
                            "amount" to 1
                        )
                        db.collection("main_ingredient").document(mainIngredient).set(number)
                    }
                }
                .addOnFailureListener { exception ->
                    Log.d("Nay", "Error getting documents: ", exception)
                }



            creAct.startActivity(intent)
        }

    }

}



