package com.foodia.recipeapp.entities

import com.google.firebase.firestore.DocumentId
import kotlinx.android.parcel.Parcelize

data class PantryItem (
    val expireDate: String = "",
    val measurement: String = "",
    val name: String = "",
    val quantity: Long = 0,
    val user: String = "",
    var purchased: Boolean = false,
    @DocumentId
    var id: String?= null
): java.io.Serializable