package com.foodia.recipeapp.entities

import java.io.Serializable

data class UserInfo (
    val firstName: String = "",
    val lastName: String = "",
    val email: String = "",
    val calories: Int = 0,
    val age: Int = 0,
    val categories: String = ""
): Serializable