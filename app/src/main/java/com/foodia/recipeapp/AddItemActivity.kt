package com.foodia.recipeapp

// Firebase Firestore
import android.content.Intent
import android.os.Bundle
import android.text.TextUtils
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.FragmentActivity
import com.foodia.recipeapp.databinding.ActivityAddItemBinding
import com.foodia.recipeapp.entities.PantryItem
import com.foodia.recipeapp.firebase.PantryCollection
import com.google.firebase.auth.ktx.auth
import com.google.firebase.firestore.SetOptions
import com.google.firebase.ktx.Firebase
import java.text.SimpleDateFormat

//import com.google.firebase.database.DatabaseReference
//import com.google.firebase.database.FirebaseDatabase

class AddItemActivity : AppCompatActivity() {

    private lateinit var binding: ActivityAddItemBinding

    private val pantryCollection = PantryCollection()

    @Deprecated("Deprecated in Java")
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == 1) {
            println("onActivityResult: ScanCodeActivity finished with result code $resultCode")
            if (resultCode == RESULT_OK) {
                val result = data?.getStringExtra("code")
                println("onActivityResult: ScanCodeActivity returned $result")
                binding.barcodeview.text = result
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityAddItemBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.buttonscan.setOnClickListener {
            val intent = Intent(this, ScanCodeActivity::class.java)
            startActivityForResult(intent, 1)
        }
        binding.additembuttontodatabase.setOnClickListener { addItem() }
    }

    // addding item to databse
    private fun addItem() {

        //data
        val nameValue =  binding.edititemname.text.toString()
        val measurementValue = binding.editcategory.text.toString()
        val quantityValue =  binding.editprice.text.toString()
        val expireDateValue = binding.editDate.dayOfMonth.toString() + "/" + binding.editDate.month.toString() + "/" + binding.editDate.year.toString()
        val itemBarcodeValue = binding.barcodeview.text.toString()


        if (itemBarcodeValue.isEmpty()) {
            binding.barcodeview.error = "It's Empty"
            binding.barcodeview.requestFocus()
            return
        }
        if (!TextUtils.isEmpty(nameValue) && !TextUtils.isEmpty(measurementValue) && !TextUtils.isEmpty(expireDateValue)&& !TextUtils.isEmpty(quantityValue)
        ) {
            val user: String = Firebase.auth.currentUser?.email?: "no user"
            val item = PantryItem(
                expireDateValue,
                measurementValue,
                nameValue,
                quantityValue.toLong(),
                user
            )
            pantryCollection.getDb().collection(pantryCollection.getCollectionName())
                .document(itemBarcodeValue)
                .set(item, SetOptions.merge())

            binding.edititemname.text.clear()
            binding.editcategory.text.clear()
            binding.editprice.text.clear()
            binding.barcodeview.text = ""


            Toast.makeText(this, "$nameValue Added", Toast.LENGTH_SHORT).show()
        } else {
            Toast.makeText(this, "Please Fill all the fields", Toast.LENGTH_SHORT)
                .show()
        }
    }

     fun changeBarcodeValue(barcodeValue: String){
        binding.barcodeview.text = barcodeValue;
    }


}