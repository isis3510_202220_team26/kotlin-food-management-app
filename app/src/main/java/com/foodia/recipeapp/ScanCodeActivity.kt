package com.foodia.recipeapp

import android.Manifest
import android.content.pm.PackageManager
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import me.dm7.barcodescanner.zxing.ZXingScannerView
import me.dm7.barcodescanner.zxing.ZXingScannerView.ResultHandler


class ScanCodeActivity : AppCompatActivity(), ResultHandler {

    var MY_PERMISSIONS_REQUEST_CAMERA = 0
    var scannerView: ZXingScannerView? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        scannerView = ZXingScannerView(this)
        setContentView(scannerView)
    }

    override fun onPause() {
        super.onPause()
        scannerView!!.stopCamera()
    }

    //    @Override
    //    protected void onResume() {
    //        super.onResume();
    //        scannerView.setResultHandler(this);
    //        scannerView.startCamera();
    //    }
    override fun onPostResume() {
        super.onPostResume()
        if (ContextCompat.checkSelfPermission(applicationContext, Manifest.permission.CAMERA)
            != PackageManager.PERMISSION_GRANTED
        ) {
            ActivityCompat.requestPermissions(
                this, arrayOf(Manifest.permission.CAMERA),
                MY_PERMISSIONS_REQUEST_CAMERA
            )
        }
        scannerView!!.setResultHandler(this)
        scannerView!!.startCamera()
    }

    override fun handleResult(rawResult: com.google.zxing.Result?) {
        if(rawResult != null) {
            val intent = intent
            println("rawResult.text: " + rawResult.text)
            intent.putExtra("code", rawResult.text)
            setResult(RESULT_OK, intent)
            finish()
        }
        onBackPressed()
    }
}