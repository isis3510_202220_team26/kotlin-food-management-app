package com.foodia.recipeapp


import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.room.Room
import com.bumptech.glide.Glide
import com.foodia.recipeapp.database.RecipeDatabase
import com.foodia.recipeapp.database.RecipeFromDatabase
import com.foodia.recipeapp.databinding.ActivityRecipesBinding
import com.google.firebase.firestore.Query
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.startActivity
import org.jetbrains.anko.uiThread


class RecipesActivity : AppCompatActivity() {

    private lateinit var binding: ActivityRecipesBinding

    var numLiked: Int = 0
    var numAll: Int = 0
    val reAct = this
    val db = Firebase.firestore
    var recetasOrdenLikes = arrayOf<Map<String, Any>>()
    var recetasOrdenNombre = arrayOf<Map<String, Any>>()
// ...


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)


        binding = ActivityRecipesBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.imageViewUserBtn.setOnClickListener {
            val intent = Intent(this, MainActivity::class.java)
            this.startActivity(intent)
        }
        binding.ShowSavedAppCompatButton.setOnClickListener {
            val intent = Intent(this, LocalRecipesActivity::class.java)
            this.startActivity(intent)
        }

        binding.AddRecipeButton.setOnClickListener {
            val intent = Intent(this, CreateRecipeActivity::class.java)
            this.startActivity(intent)
        }

        val intent = Intent(this, DetailActivity::class.java)
        loadRecipes(intent)

    }

     fun loadRecipes(intent: Intent) {

        doAsync {

            db.collection("recipes")
                .orderBy("numberOfLikes", Query.Direction.DESCENDING)
                .get()
                .addOnSuccessListener { result ->
                    for (document in result) {
                        recetasOrdenLikes = recetasOrdenLikes.plus(document.data)
                        Log.d("Yay", "${document.id} => ${document.data} ")
                        Log.d("Anadido", "${recetasOrdenLikes[0]}")
                    }
                    Log.d("array", "${recetasOrdenLikes[0]["title"]} ")
                    changeLikedRecipe(numLiked)
                    binding.imageViewPreviousLiked.visibility = View.INVISIBLE

                    binding.imgRecipe.setOnClickListener {

                        intent.putExtra("name", recetasOrdenLikes[numLiked]["title"].toString())
                        intent.putExtra(
                            "description",
                            recetasOrdenLikes[numLiked]["description"].toString()
                        )
                        intent.putExtra(
                            "likes",
                            recetasOrdenLikes[numLiked]["numberOfLikes"].toString()
                        )
                        intent.putExtra("url", recetasOrdenLikes[numLiked]["imageURL"].toString())
                        reAct.startActivity(intent)
                    }
                    binding.imageViewNextLiked.setOnClickListener {
                        numLiked++
                        if (numLiked == 4) {
                            binding.imageViewNextLiked.visibility = View.INVISIBLE
                            changeLikedRecipe(numLiked)
                        } else {
                            binding.imageViewPreviousLiked.visibility = View.VISIBLE
                            binding.imageViewNextLiked.visibility = View.VISIBLE
                            changeLikedRecipe(numLiked)
                        }
                    }
                    binding.imageViewPreviousLiked.setOnClickListener {
                        numLiked--
                        if (numLiked == 0) {
                            binding.imageViewPreviousLiked.visibility = View.INVISIBLE
                            changeLikedRecipe(numLiked)
                        } else {
                            binding.imageViewPreviousLiked.visibility = View.VISIBLE
                            binding.imageViewNextLiked.visibility = View.VISIBLE
                            changeLikedRecipe(numLiked)
                        }
                    }

                }
                .addOnFailureListener { exception ->
                    Log.d("Nay", "Error getting documents: ", exception)
                }

        }
        doAsync {
            db.collection("recipes")
                .orderBy("title", Query.Direction.ASCENDING)
                .get()
                .addOnSuccessListener { result ->
                    for (document in result) {
                        recetasOrdenNombre = recetasOrdenNombre.plus(document.data)
                        Log.d("Tiki", document.data["title"].toString())


                    }
                    Log.d("arrayNombre", "${recetasOrdenNombre[0]["title"]} ")
                    var numRecetasTotal: Int = recetasOrdenNombre.size
                    Log.d("Num recetas", "$numRecetasTotal")
                    changeRecipeAll(numAll)
                    binding.imageViewPreviousAll.visibility = View.INVISIBLE

                    binding.imgRecipeAll.setOnClickListener {

                        intent.putExtra("name", recetasOrdenNombre[numAll]["title"].toString())
                        intent.putExtra(
                            "description",
                            recetasOrdenNombre[numAll]["description"].toString()
                        )
                        intent.putExtra(
                            "likes",
                            recetasOrdenNombre[numAll]["numberOfLikes"].toString()
                        )
                        intent.putExtra("url", recetasOrdenNombre[numAll]["imageURL"].toString())
                        reAct.startActivity(intent)
                    }

                    binding.imageViewNextAll.setOnClickListener {
                        numAll++
                        if (numAll == (numRecetasTotal - 1)) {
                            binding.imageViewNextAll.visibility = View.INVISIBLE
                            changeRecipeAll(numAll)
                        } else {
                            binding.imageViewPreviousAll.visibility = View.VISIBLE
                            binding.imageViewNextAll.visibility = View.VISIBLE
                            changeRecipeAll(numAll)
                            Log.d("Num recetas", "$numAll")
                        }
                    }
                    binding.imageViewPreviousAll.setOnClickListener {
                        numAll--
                        if (numAll == 0) {
                            binding.imageViewPreviousAll.visibility = View.INVISIBLE
                            changeRecipeAll(numAll)
                        } else {
                            binding.imageViewPreviousAll.visibility = View.VISIBLE
                            binding.imageViewNextAll.visibility = View.VISIBLE
                            changeRecipeAll(numAll)
                        }
                    }
                    uiThread {
                        doAsync {
                            val dbRoom = Room.databaseBuilder(
                                applicationContext,
                                RecipeDatabase::class.java, "recipe-database"
                            ).build()

                            for (recipes in recetasOrdenLikes) {
                                dbRoom.recipeDao().insert(
                                    RecipeFromDatabase(
                                        null,
                                        recipes["title"].toString(),
                                        recipes["description"].toString(),
                                        recipes["numberOfLikes"].toString().toInt(),
                                        recipes["imageURL"].toString(),
                                        recipes["dateCreated"].toString()
                                    )
                                )
                            }
                            val recipes = dbRoom.recipeDao().getAll()
                            Log.d("toko", "num recipes ${recipes[0].title} ")
                        }
                    }


                }
                .addOnFailureListener { exception ->
                    Log.d("Nay", "Error getting documents: ", exception)
                }



        }

    }

    private fun changeLikedRecipe(i: Int) {
        binding.textViewTitle.text = recetasOrdenLikes[i]["title"].toString()
        binding.textViewLikes.text =
            "Liked by ${recetasOrdenLikes[i]["numberOfLikes"].toString()} Foodies"
        Glide.with(binding.imgRecipe.context).load(recetasOrdenLikes[i]["imageURL"])
            .into(binding.imgRecipe)
    }

    private fun changeRecipeAll(i: Int) {
        binding.textViewRecipeAll.text = recetasOrdenNombre[i]["title"].toString()
        Glide.with(binding.imgRecipeAll.context).load(recetasOrdenNombre[i]["imageURL"])
            .into(binding.imgRecipeAll)
    }


}