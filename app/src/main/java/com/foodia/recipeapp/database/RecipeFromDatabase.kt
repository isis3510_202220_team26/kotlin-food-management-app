package com.foodia.recipeapp.database

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class RecipeFromDatabase(

    @PrimaryKey(autoGenerate = true) val uid: Int?,
    @ColumnInfo(name = "title") val title: String,
    @ColumnInfo(name = "description") val description: String,
    @ColumnInfo(name = "number_of_likes") val numberOfLikes: Int,
    @ColumnInfo(name = "url") val imageURL: String,
    @ColumnInfo(name = "date_created") val dateCreated: String,
)