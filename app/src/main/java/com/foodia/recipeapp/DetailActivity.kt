package com.foodia.recipeapp

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.bumptech.glide.Glide
import com.foodia.recipeapp.databinding.ActivityDetailBinding
import android.content.Context
import android.content.SharedPreferences
import android.widget.Toast
import kotlin.math.log


class DetailActivity : AppCompatActivity() {

    private lateinit var binding: ActivityDetailBinding


// ...


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityDetailBinding.inflate(layoutInflater)
        setContentView(binding.root)

        val sharedPrefFile = "kotlinsharedpreference"

        val extras = intent.extras
        if (extras != null) {
            val name = extras.getString("name")
            val description = extras.getString("description")
            val likes = extras.getString("likes")
            val url = extras.getString("url")

            binding.textViewName.text = name
            binding.textViewDescription.text = description
            binding.textViewLikes.text = "Liked by ${likes} Foodies"
            Glide.with(binding.imgItem.context).load(url).into(binding.imgItem)

            val sharedPreferences: SharedPreferences = this.getSharedPreferences(sharedPrefFile,Context.MODE_PRIVATE)
            binding.SaveLocallyAppCompatButton.setOnClickListener{
                val editor:SharedPreferences.Editor =  sharedPreferences.edit()
                editor.putString(name,description)
                editor.apply()
                editor.commit()
                val toast = Toast.makeText(this, "Recipe saved locally", Toast.LENGTH_LONG)
                toast.show()

            }

        }
    }

}