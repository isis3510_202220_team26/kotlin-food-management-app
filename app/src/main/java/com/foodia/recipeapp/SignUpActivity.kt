package com.foodia.recipeapp

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import com.foodia.recipeapp.shared.isNetworkConnected
import android.os.Bundle
import android.util.Patterns
import android.widget.Toast
import com.foodia.recipeapp.databinding.ActivitySignUpBinding
import com.foodia.recipeapp.firebase.UsersCollection
import com.google.firebase.auth.FirebaseAuth

import com.google.firebase.auth.ktx.auth
import com.google.firebase.ktx.Firebase
import java.util.regex.Pattern

class SignUpActivity : AppCompatActivity() {

    private lateinit var auth: FirebaseAuth
    private lateinit var binding: ActivitySignUpBinding
    private val userCollection = UsersCollection()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivitySignUpBinding.inflate(layoutInflater)
        setContentView(binding.root)
        auth = Firebase.auth

        binding.signUpButton.setOnClickListener {
            if(isNetworkConnected){
                val mEmail = binding.emailEditText.text.toString()
                val mPassword = binding.passwordEditText.text.toString()
                val mRepeatPassword = binding.repeatPasswordEditText.text.toString()

                val passwordRegex = Pattern.compile("^" +
                        "(?=.*[-@#$%^&+=])" +     // Al menos 1 carácter especial
                        ".{6,}" +                // Al menos 4 caracteres
                        "$")

                if(mEmail.isEmpty() || !Patterns.EMAIL_ADDRESS.matcher(mEmail).matches()) {
                    Toast.makeText(this, "Ingrese un email valido.",
                        Toast.LENGTH_SHORT).show()
                } else if (mPassword.isEmpty() || !passwordRegex.matcher(mPassword).matches()){
                    Toast.makeText(this, "La contraseña es debil.",
                        Toast.LENGTH_SHORT).show()
                } else if (mPassword != mRepeatPassword){
                    Toast.makeText(this, "Confirma la contraseña.",
                        Toast.LENGTH_SHORT).show()
                } else {
                    createAccount(mEmail, mPassword)
                }
            }else{
                Toast.makeText(this, "No hay conexíon a internet.",
                    Toast.LENGTH_SHORT).show()
            }


        }

        binding.backImageView.setOnClickListener {
            val intent = Intent(this, SignInActivity::class.java)
            this.startActivity(intent)
        }

    }

    public override fun onStart() {
        super.onStart()
        val currentUser = auth.currentUser
        if(currentUser != null){
            if(currentUser.isEmailVerified){
                val intent = Intent(this, MainActivity::class.java)
                this.startActivity(intent)
            } else {
                val intent = Intent(this, CheckEmailActivity::class.java)
                this.startActivity(intent)
            }
        }
    }

    private fun createAccount(email: String, password: String) {
        auth.createUserWithEmailAndPassword(email, password)
            .addOnCompleteListener(this) { task ->
                if (task.isSuccessful) {
                    val intent = Intent(this, CheckEmailActivity::class.java)
                    this.startActivity(intent)
                    userCollection.createUser(email)
                } else {
                    Toast.makeText(this, "No se pudo crear la cuenta. Vuelva a intertarlo",
                        Toast.LENGTH_SHORT).show()
                }
            }
    }

}